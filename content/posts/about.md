---
title: "About"
date: 2018-03-20T10:10:39+10:00
draft: false
weight: 1
type: "post"
class: "post first"
---

Opie Robots bring digital language resources to life and excite children about Indigenous culture. They provide language activities in the form of interactive stories, words games and pronunciation practice. It’s a new technology for the world’s oldest languages, supporting language revival, and growing digital literacy around cultural knowledge.